import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLoaderComponent } from './page-loader/page-loader.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { MultifileUploadComponent } from './multifile-upload/multifile-upload.component';
import { MultifiletableuploadComponent } from './multifiletableupload/multifiletableupload.component';


@NgModule({
  declarations: [
    PageLoaderComponent,
    FileUploadComponent,
    MultifileUploadComponent,
    MultifiletableuploadComponent
  ],
  imports: [
    CommonModule,
    NgxSpinnerModule,
    NgxDropzoneModule,
  ],
  exports:[
    PageLoaderComponent,
    FileUploadComponent,
    MultifileUploadComponent,
    MultifiletableuploadComponent
  ]
})
export class ComponentsModule { }
