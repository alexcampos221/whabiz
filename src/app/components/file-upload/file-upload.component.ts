import { Input } from '@angular/core';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Utils } from 'src/app/utils';
import { FirebaseStorageService } from '../../services/firebase-storage.service';
import { SweetalertsService } from '../../services/sweetalerts.service';

@Component({
  selector: 'vex-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {
  @Output() change: EventEmitter<any> = new EventEmitter<any>();
  @Output() removed: EventEmitter<any> = new EventEmitter<any>();
  @Input('label') label: any;
  @Input('filePath') filePath: any;
  @Input('formats') formats: any;
  @Input('maxFiles') maxFiles: any;
  @Input('id') id: any;


  files: File[] = [];
  fileEvent;
  currentFileExtension="";
  currentFileIsImage=false;
  imagePath;
  imgURL;
  constructor(
    private firebaseStorageService: FirebaseStorageService,
    public sweetalertsService: SweetalertsService,
  ) { }

  ngOnInit(): void {
  }
  changeFile(event){
    let formatAvailable=Utils.getFileExtension((event.addedFiles[0].name).toLowerCase());
    if(this.formats?.includes(formatAvailable) || this.formats==undefined){
      if(this.files.length<this.maxFiles){
        this.files = [];
        this.files.push(...event.addedFiles);
        this.fileEvent=event;
        this.currentFileExtension=Utils.getFileExtension((this.fileEvent.addedFiles[0].name).toLowerCase());
        this.currentFileIsImage=Utils.getFileType(this.fileEvent.addedFiles[0].type);
        console.info("currentFileExtension:currentFileExtension: ",this.currentFileExtension);
        console.info("EVENT: ",event);

        var reader = new FileReader();
        this.imagePath = this.files;
        reader.readAsDataURL(this.files[0]); 
        reader.onload = (_event) => { 
          this.imgURL = reader.result; 
        }
        this.change.emit(event);
      }else{
        this.sweetalertsService.generalWarning("Solo se permite "+this.maxFiles+" archivo(s)");
      }
    }else{
      this.sweetalertsService.generalWarning("Formato no aceptado");
    }
  }
  removeFile(event){
    this.files.splice(this.files.indexOf(event), 1);
    this.removed.emit(event);
  }
  agregarAdjunto(filePath){
    this.firebaseStorageService.uploadFile(this.files,filePath).then((publicURL:any)=>{ 
      this.saveReference(this.fileEvent.addedFiles[0].name, Utils.getFileExtension(this.fileEvent.addedFiles[0].name), this.fileEvent.addedFiles[0].type, publicURL);
      console.info("publicYEAH: ",publicURL);
    })
  }
  private saveReference(nombre, extension, tipo, ruta){
    // this.adjuntoActividad.controls['tareaID'].setValue(this.kanbanCurrentUpdateItem.tar_id);
    // this.adjuntoActividad.controls['nombre'].setValue(nombre);
    // this.adjuntoActividad.controls['extension'].setValue(extension);
    // this.adjuntoActividad.controls['type'].setValue(tipo);
    // this.adjuntoActividad.controls['ruta'].setValue(ruta);
    // this.adjuntoActividad.controls['columnaNombre'].setValue(this.kanbanCurrentUpdateItem.colk_nombre);
    // this.adjuntoActividad.controls['columnaTema'].setValue(this.kanbanCurrentUpdateItem.colk_thema);
    // console.info("agregarAdjunto: ", this.adjuntoActividad.getRawValue());
    // this.kanbanService.agregarAdjunto(this.adjuntoActividad.getRawValue()).subscribe((resp)=>{
    //   this.listarAdjuntos();
    //   this.kanbanCurrentUpdateItem.tar_qty_adjuntos=+this.kanbanCurrentUpdateItem.tar_qty_adjuntos+1;
    //   this.toastr.success('Adjunto subido!', 'Success!', { timeOut: 3000 });
    //   this.filesmultimedia = [];
    //   this.addInLineFileFlag=false;
    //   this.adjuntoActividad.reset({fechaEjecucion:Global.dateFormValue()});
    //   console.info("agregarAdjuntoRESP: ",resp);
    // },(error)=>{
    //   this.sweetalertsService.generalError("No se pudo agregar el adjunto");
    // })
  }
}
