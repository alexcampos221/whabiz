import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultifiletableuploadComponent } from './multifiletableupload.component';

describe('MultifiletableuploadComponent', () => {
  let component: MultifiletableuploadComponent;
  let fixture: ComponentFixture<MultifiletableuploadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MultifiletableuploadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MultifiletableuploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
