import { Component, OnInit } from '@angular/core';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'vex-page-loader',
  templateUrl: './page-loader.component.html',
  styleUrls: ['./page-loader.component.scss']
})
export class PageLoaderComponent implements OnInit {
  opacity=1;
  message="";
  constructor(
    public loadingService: LoadingService
  ) {
    this.loadingService.loading$.subscribe((resp)=>{
      this.message=resp['message'];
      this.opacity=resp['opacity'];
    })
  }
  ngOnInit() {}
}
