import home from '@iconify/icons-ic/home';
import groupAdd from '@iconify/icons-ic/group-add'
export const appIcons = {
    home,
    'group-add': groupAdd
}