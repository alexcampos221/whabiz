import { Utils } from "../utils";

export class Adjunto {
    nombre:string;
    extension:string;
    tipo:string;
    ruta:string;
    constructor(data?,publicURL?) {
        this.nombre=data.addedFiles[0].name || '';
        this.extension=Utils.getFileExtension(data.addedFiles[0].name) || '';
        this.tipo=data.addedFiles[0].type || '';
        this.ruta=publicURL|| '';
    }
}
