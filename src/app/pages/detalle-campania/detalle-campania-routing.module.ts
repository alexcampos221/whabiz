import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetalleCampaniaComponent } from './detalle-campania.component';

const routes: Routes = [
  {
    path: '',
    component: DetalleCampaniaComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetalleCampaniaRoutingModule { }
