import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Component, ComponentFactory, ComponentFactoryResolver, ComponentRef, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { RxStompService } from '@stomp/ng2-stompjs';
import { fadeInUp400ms } from 'src/@vex/animations/fade-in-up.animation';
import { stagger60ms } from 'src/@vex/animations/stagger.animation';

@Component({
  selector: 'vex-detalle-campania',
  templateUrl: './detalle-campania.component.html',
  styleUrls: ['./detalle-campania.component.scss'],
  animations: [
    stagger60ms,
    fadeInUp400ms
  ]
})
export class DetalleCampaniaComponent implements OnInit {
  @ViewChild("containerStep", { read: ViewContainerRef }) containerStep;
  containerStepRef: ComponentRef<any>;
  stepsData={
    step:"list",
    data:undefined
  }
  constructor(
    public rxStompService:RxStompService
  ) { 

  }

  ngOnInit(): void {
    this.rxStompService.watch('/topic/greeting').subscribe((message) => {
      console.info("topic/greeting: ",message.body);
      // this.receivedMessages.push(message.body);
    });
  }
  
  openList(){
    ////STEP CHANGES////
    this.stepsData.step="list";this.stepsData.data=undefined;
    ////STEP CHANGES////
    this.containerStepRef?this.containerStepRef.destroy():null;
  }
  // openDevice() {
  //   this.matDialog.open(DeviceComponent, {
  //     data: null,
  //     width: '600px'
  //   });
  // }
}
