import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { fadeInUp400ms } from 'src/@vex/animations/fade-in-up.animation';
import { stagger60ms } from 'src/@vex/animations/stagger.animation';
import { EditProfileComponent } from 'src/app/pages/sincronizar/components/edit-profile/edit-profile.component';
import { SincronizarService } from 'src/app/services/sincronizar.service';
import { SocketService } from 'src/app/services/socket.service';
import { SweetalertsService } from 'src/app/services/sweetalerts.service';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { WhatsappService } from 'src/app/services/whatsapp.service';
// import { AutoUnsubscribe } from "ngx-auto-unsubscribe";
import * as Global from '../../../../globals';

// @AutoUnsubscribe()
@Component({
  selector: 'vex-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.scss'],
  animations: [
    stagger60ms,
    fadeInUp400ms
  ]
})
export class DeviceComponent implements OnInit {
  @Output() openSincronizeEmitter = new EventEmitter<string>();
  @Input() deviceData;
  infoStatus;
  currentSession;
  clientState;
  authenticationInProgress=false;
  socketClient;
  public whatsappTokenInstance : FormGroup;
  public whatsappSaveInstance : FormGroup;
  public envioForm: FormGroup;
  private events$Obs;
  private getStatus$Obs;
  private restoreClient$Obs;
  constructor(
    public sincronizarService: SincronizarService,
    private matDialog: MatDialog,
    public socketService: SocketService,
    public formBuilder: FormBuilder,
    public sweetalertsService: SweetalertsService,
    public usuariosService: UsuariosService,
    public whatsappService: WhatsappService
  ) {
    this.whatsappTokenInstance = this.formBuilder.group({
      WABrowserId: [''],
      WASecretBundle: [''],
      WAToken1: [''],
      WAToken2: [''],
    });
    this.whatsappSaveInstance = this.formBuilder.group({
      instancia: [''],
      token: [''],
      fechaHora: [''],
      hash: ['']
    });
    this.envioForm = this.formBuilder.group({
      mensaje: [''],
      numeroUno: [''],
      numeroDos: ['']
    });
    this.events$Obs=this.socketService.events$.subscribe((event:any)=>{
      console.warn("socketService.events$: ",event);
      switch (event.event){
        case "auth_failure":
          this.clientState='auth_failure';
        break;
        case "authenticated":
          this.clientState='authenticated';
          this.currentSession=event.data;
          console.info("EVENTauthenticated: ",event);
          event.data?this.sweetalertsService.generalSuccess("Sincronizado"):this.sweetalertsService.generalError("Algo fallo.");
        break;
        case "change_battery":
          this.clientState='change_battery';
        break;
        case "change_state":
          this.infoStatus?this.infoStatus.data._state=event.data:null;
          this.clientState='change_state';
        break;
        case "disconnected":
          this.infoStatus?this.infoStatus.data._state=event.data:null;
          this.clientState="disconnected";
        break;
        case "group_join":
          this.clientState='group_join';
        break;
        case "group_leave":
          this.clientState='group_leave';
        break;
        case "group_update":
          this.clientState='group_update';
        break;
        case "incoming_call":
          this.clientState='incoming_call';
        break;
        case "media_uploaded":
          this.clientState='media_uploaded';
        break;
        case "message":
          this.clientState='message';
        break;
        case "message_ack":
          this.clientState='message_ack';
        break;
        case "message_create":
          this.clientState='message_create';
        break;
        case "message_revoke_everyone":
          this.clientState='message_revoke_everyone';
        break;
        case "message_revoke_me":
          this.clientState='message_revoke_me';
        break;
        case "ready":
          this.clientState='ready';
        break;
        ///EXTRA
        case "close_reason":
          this.clientState=event.data;
        break;
      }
          
      console.info("events: ",event);
    })
   }

  ngOnInit(): void {
    this.getStatus();
  }
  getStatus(currentSession?){
    this.getStatus$Obs=this.sincronizarService.getStatus(this.deviceData.instancia_url, currentSession).subscribe((resp)=>{
      this.infoStatus=resp;
      console.info("getStatus: ",resp );
    })
  }
  openEdit() {
    this.matDialog.open(EditProfileComponent, {
      data: null,
      width: '600px'
    });
  }
  restoreClient(){
    this.restoreClient$Obs=this.sincronizarService.restoreClient(this.deviceData.instancia_url,{}).subscribe((resp)=>{
      this.sweetalertsService.generalSuccess("Cliente restaurado con exito");
      this.socketClient=this.socketService.init();
    },(error)=>{
      this.sweetalertsService.generalError("Fallo restaurando la sesion, vuelva a sincronizar");
    })
  }
  logout(){
    this.restoreClient$Obs=this.sincronizarService.logout(this.deviceData.instancia_url,{}).subscribe((resp)=>{
      this.sweetalertsService.generalSuccess("Cliente finalizado");
      this.socketClient=this.socketService.init();
    },(error)=>{
      this.sweetalertsService.generalError("Fallo restaurando la sesion, vuelva a sincronizar");
    })
  }
  openSincronize(){
    // console.info(this.whatsappSaveInstance.getRawValue());
    this.openSincronizeEmitter.emit(this.whatsappSaveInstance.getRawValue());
  }
  probar(){
    let message = this.envioForm.controls.mensaje.value!=''?this.envioForm.controls.mensaje.value:"mensaje de prueba de "+this.deviceData.numero;
    console.warn("message: ",message);
    if(this.envioForm.controls.numeroUno.value!=''){
      let body={
        to:this.envioForm.controls.numeroUno.value,
        msg:message
      }
      console.info("bodyUno: ",body);
      this.sendMessageText(body);
    }
    if(this.envioForm.controls.numeroDos.value!=''){
      let body={
        to:this.envioForm.controls.numeroDos.value,
        msg:message
      }
      console.info("bodyDos: ",body);
      this.sendMessageText(body);
    }
  }
  sendMessageText(body){
    this.whatsappService.sendMessageText(body).subscribe((success)=>{
      console.info("sendMessageTextSUCCESS: ",success);
    },(error)=>{
      console.info("sendMessageTextERROR: ",error);
    });
  }
  ngOnDestroy() {
    this.events$Obs?this.events$Obs.unsubscribe():null;
    this.getStatus$Obs?this.getStatus$Obs.unsubscribe():null;
    this.restoreClient$Obs?this.restoreClient$Obs.unsubscribe():null;
  }
}
