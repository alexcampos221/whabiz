import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { fadeInUp400ms } from 'src/@vex/animations/fade-in-up.animation';
import { stagger60ms } from 'src/@vex/animations/stagger.animation';
import { TableColumn } from 'src/@vex/interfaces/table-column.interface';
import { dummyDevices } from 'src/static-data/dummy-devices';

@Component({
  selector: 'vex-list-device',
  templateUrl: './list-device.component.html',
  styleUrls: ['./list-device.component.scss'],
  animations: [
    stagger60ms,
    fadeInUp400ms
  ]
})
export class ListDeviceComponent implements OnInit {
  @Output() openDeviceEmitter = new EventEmitter<string>();
  @Input()
  columns: TableColumn<any>[] = [
    { label: 'Image', property: 'image', type: 'image', visible: true },
    { label: 'Numero', property: 'numero', type: 'text', visible: true },
    { label: 'Estado', property: 'estado', type: 'text', visible: true },
    { label: 'Plan', property: 'plan', type: 'text', visible: true },
    { label: 'Facturacion', property: 'facturacion', type: 'text', visible: true },
    { label: 'Estadisticas', property: 'estadisticas', type: 'text', visible: true },
    { label: 'Acciones', property: 'acciones', type: 'text', visible: true }
  ];
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<any> | null;
  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }
  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }
  constructor(
    
  ) {
    this.dataSource = new MatTableDataSource();
    this.dataSource.data = dummyDevices
   }

  ngOnInit(): void {
  }
  openDevice(device){
    console.info(device);
    this.openDeviceEmitter.emit(device);
  }
}
