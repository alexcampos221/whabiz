import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SincronizeComponent } from './sincronize.component';

describe('SincronizeComponent', () => {
  let component: SincronizeComponent;
  let fixture: ComponentFixture<SincronizeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SincronizeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SincronizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
