import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SincronizarService } from 'src/app/services/sincronizar.service';
import { SocketService } from 'src/app/services/socket.service';
import { SweetalertsService } from 'src/app/services/sweetalerts.service';
import { UsuariosService } from 'src/app/services/usuarios.service';
import icCry from '@iconify/icons-fa-solid/sad-cry';
import { fadeInUp400ms } from 'src/@vex/animations/fade-in-up.animation';
import { stagger60ms } from 'src/@vex/animations/stagger.animation';
// import { AutoUnsubscribe } from "ngx-auto-unsubscribe";
import * as Global from '../../../../globals';

// @AutoUnsubscribe()
@Component({
  selector: 'vex-sincronize',
  templateUrl: './sincronize.component.html',
  styleUrls: ['./sincronize.component.scss'],
  animations: [
    stagger60ms,
    fadeInUp400ms
  ]
})
export class SincronizeComponent implements OnInit {
  @Output() sincronizeEventEmitter = new EventEmitter<string>();
  @Input() deviceData;
  icCry = icCry;
  qr;
  clientState;
  socketClient;
  currentSession;
  public whatsappSaveInstance : FormGroup;
  private sincronize$Obs;
  private qrEvent$Obs;
  private events$Obs;
  constructor(
    public formBuilder: FormBuilder,
    public sweetalertsService:SweetalertsService,
    public sincronizarService:SincronizarService,
    public socketService:SocketService,
    public usuariosService:UsuariosService
  ) { 
    this.whatsappSaveInstance = this.formBuilder.group({
      instancia: [''],
      token: [''],
      fechaHora: [''],
      hash: ['']
    });
    this.events$Obs=this.socketService.events$.subscribe((event:any)=>{
      console.warn("socketService.events$: ",event);
      switch (event.event){
        case "auth_failure":
          this.clientState='auth_failure';
        break;
        case "authenticated":
          this.clientState='authenticated';
          this.currentSession=event.data;
          console.info("currentSession: ",this.currentSession);
          event.data?this.sweetalertsService.generalSuccess("Sincronizado"):this.sweetalertsService.generalError("Algo fallo.");
        break;
        case "disconnected":
          this.clientState='disconnected';
        break;
        case "qr":
          this.clientState="qrReceived";
          this.sweetalertsService.dissmissSwal();
          this.qr=event.data;
        break;
        case "ready":
          this.clientState='ready';
        break;
        case "qr_expired":
          if(event.data=="timeout"){
            this.clientState="timeout";
            this.qr=undefined;
          }//puede descronbectarse primero por socket.dissconect() y no recibir timeout reason
        break;  
      }
      // this.sincronizeEventEmitter.emit(event.event);
      console.info("events: ",event);
    })
  }

  ngOnInit(): void {
    this.sincronize();
  }
  ngOnDestroy() {
    console.warn("DESTROYSINCRONIZAR");
    if(this.sincronize$Obs){
      console.info("this.sincronize$Obs: ",this.sincronize$Obs);
      this.sincronize$Obs.unsubscribe();
    }else{
      console.info("No se pudo unsubcribe this.sincronize$Obs");
    }
    if(this.qrEvent$Obs){
      console.info("this.qrEvent$Obs: ",this.qrEvent$Obs);
      this.qrEvent$Obs.unsubscribe();
    }else{
      console.info("No se pudo unsubcribe this.qrEvent$Obs");
    }
    if(this.events$Obs){
      console.info("this.events$Obs: ",this.events$Obs);
      this.events$Obs.unsubscribe();
    }else{
      console.info("No se pudo unsubcribe this.events$Obs");
    }
    this.socketService.end();
  }
  sincronize(){
    console.info("sincronize");
    this.clientState="sincronize";
    this.sweetalertsService.loading();
    this.socketService.init(this.deviceData.instancia_url);
    this.sincronize$Obs=this.sincronizarService.sincronize(this.deviceData.instancia_url).subscribe((resp)=>{
      this.sincronizeEventEmitter.emit(resp);//send status al componente device
    },(error)=>{
      console.error("FALLOQR");
      this.sweetalertsService.generalError("Fallo obteniendo QR");
    })
  }
}
