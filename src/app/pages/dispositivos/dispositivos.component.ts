import { Component, ComponentFactory, ComponentFactoryResolver, ComponentRef, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { fadeInUp400ms } from 'src/@vex/animations/fade-in-up.animation';
import { stagger60ms } from 'src/@vex/animations/stagger.animation';
import { DeviceComponent } from './components/device/device.component';
import { SincronizeComponent } from './components/sincronize/sincronize.component';

@Component({
  selector: 'vex-dispositivos',
  templateUrl: './dispositivos.component.html',
  styleUrls: ['./dispositivos.component.scss'],
  animations: [
    stagger60ms,
    fadeInUp400ms
  ]
})
export class DispositivosComponent implements OnInit {
  @ViewChild("containerStep", { read: ViewContainerRef }) containerStep;
  containerStepRef: ComponentRef<any>;
  stepsData={
    step:"list",
    data:undefined
  }
  constructor(
    private componentFactoryResolver: ComponentFactoryResolver
  ) { 

  }

  ngOnInit(): void {
  }
  
  openDevice(deviceListData,status?) {
    this.instanceTemplate(DeviceComponent);
    this.containerStepRef.instance.deviceData=deviceListData;
    this.containerStepRef.instance.openSincronizeEmitter.subscribe((deviceData)=>{
      this.openSincronize({...deviceData,...deviceListData});
    })
    if(status){
      this.containerStepRef.instance.infoStatus=status;
    }
    ////STEP CHANGES////
    this.stepsData.step="device";this.stepsData.data=deviceListData;
    ////STEP CHANGES////
  }
  openSincronize(deviceData){
    this.instanceTemplate(SincronizeComponent);
    this.containerStepRef.instance.deviceData=deviceData;
    this.containerStepRef.instance.sincronizeEventEmitter.subscribe((status)=>{
      this.openDevice(deviceData,status);
    })
    ////STEP CHANGES////
    this.stepsData.step="sincronize";this.stepsData.data=deviceData;
    ////STEP CHANGES////
  }
  openList(){
    ////STEP CHANGES////
    this.stepsData.step="list";this.stepsData.data=undefined;
    ////STEP CHANGES////
    this.containerStepRef?this.containerStepRef.destroy():null;
  }
  // openDevice() {
  //   this.matDialog.open(DeviceComponent, {
  //     data: null,
  //     width: '600px'
  //   });
  // }
  private instanceTemplate(component){
    this.containerStepRef?this.containerStepRef.destroy():null;
    this.containerStep.clear();
    const factory: ComponentFactory<any> = this.componentFactoryResolver.resolveComponentFactory(component);
    this.containerStepRef = this.containerStep.createComponent(factory);
  }
}
