import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DispositivosRoutingModule } from './dispositivos-routing.module';
import { ComponentsModule } from 'src/app/components/components.module';
import { DispositivosComponent } from './dispositivos.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { IconModule } from '@visurel/iconify-angular';
import { QRCodeModule } from 'angularx-qrcode';
import { BreadcrumbsModule } from 'src/@vex/components/breadcrumbs/breadcrumbs.module';
import { PageLayoutModule } from 'src/@vex/components/page-layout/page-layout.module';
import { SecondaryToolbarModule } from 'src/@vex/components/secondary-toolbar/secondary-toolbar.module';
import { ContainerModule } from 'src/@vex/directives/container/container.module';
import { CustomerCreateUpdateModule } from '../apps/aio-table/customer-create-update/customer-create-update.module';
import { EditProfileModule } from '../sincronizar/components/edit-profile/edit-profile.module';
import { DeviceComponent } from './components/device/device.component';
import { ListDeviceComponent } from './components/list-device/list-device.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { SincronizeComponent } from './components/sincronize/sincronize.component';

@NgModule({
  declarations: [DispositivosComponent,ListDeviceComponent,DeviceComponent, SincronizeComponent],
  imports: [
    CommonModule,
    MatListModule,
    IconModule,
    DispositivosRoutingModule,
    SecondaryToolbarModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    CustomerCreateUpdateModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatCheckboxModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    IconModule,
    FormsModule,
    MatTooltipModule,
    ReactiveFormsModule,
    ContainerModule,
    MatSelectModule,
    MatButtonToggleModule,
    QRCodeModule,
    EditProfileModule,
    ComponentsModule,
    MatProgressBarModule
  ],
  exports:[ListDeviceComponent,DeviceComponent]
})
export class DispositivosModule { }
