import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuicklinkModule } from 'ngx-quicklink';
import { VexRoutes } from 'src/@vex/interfaces/vex-route.interface';
import { EnvioPersonalizadoComponent } from './envio-personalizado.component';

const routes: VexRoutes = [
  {
    path: '',
    component: EnvioPersonalizadoComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule, QuicklinkModule]
})
export class EnvioPersonalizadoRoutingModule { }
