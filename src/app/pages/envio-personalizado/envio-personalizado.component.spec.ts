import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnvioPersonalizadoComponent } from './envio-personalizado.component';

describe('EnvioPersonalizadoComponent', () => {
  let component: EnvioPersonalizadoComponent;
  let fixture: ComponentFixture<EnvioPersonalizadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnvioPersonalizadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnvioPersonalizadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
