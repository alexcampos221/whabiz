import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { fadeInUp400ms } from 'src/@vex/animations/fade-in-up.animation';
import { stagger60ms } from 'src/@vex/animations/stagger.animation';
import { SincronizarService } from 'src/app/services/sincronizar.service';
import { SocketService } from 'src/app/services/socket.service';
import { SweetalertsService } from 'src/app/services/sweetalerts.service';
import { WhatsappService } from 'src/app/services/whatsapp.service';
// import { QRCode } from
import icBeenhere from '@iconify/icons-ic/twotone-beenhere';
import icStars from '@iconify/icons-ic/twotone-stars';
import icBusinessCenter from '@iconify/icons-ic/twotone-business-center';
import icPhoneInTalk from '@iconify/icons-ic/twotone-phone-in-talk';
import icMail from '@iconify/icons-ic/twotone-mail';
import icCry from '@iconify/icons-fa-solid/sad-cry';
import { FirebaseStorageService } from 'src/app/services/firebase-storage.service';
import { Utils } from 'src/app/utils';
import { Adjunto } from 'src/app/models/adjunto.models';
import { dummyDevices } from 'src/static-data/dummy-devices';
import { base64StringToBlob } from 'blob-util';

import * as Global from '../../globals';
import * as XLSX from 'xlsx';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'vex-envio-personalizado',
  templateUrl: './envio-personalizado.component.html',
  styleUrls: ['./envio-personalizado.component.scss'],
  animations: [
    stagger60ms,
    fadeInUp400ms
  ]
})
export class EnvioPersonalizadoComponent implements OnInit {
  Utils=Utils;
  ///VEX-FILE-UPLOAD
  multimediaData="0";
  filesprueba: File[] = [];filescampana: File[] = [];filesmultimedia: File[] = [];
  adjMultimedia;
  adjuntoActividad: FormGroup;
  _adjuntoCurrentFile;
  _adjuntoImagen;
  _adjuntosList=[];
  _defaultRow=1;
  //READER/////////////////
  arrayBuffer;
  firstRow;
  fromTo;
  messagesArray;
  excelHeaders;
  senderRowsCount;
  excelValidatorErrors=[];
  senderMessageCount=0;
  metricSuccessMessages=0;
  metricFailMessages=0;
  metricSuccessSendedMessages=0;
  metricSuccessEnqueueMessages=0;
  private fileContainer = new FormData();
  adjCampania;
  messagePattern: FormGroup;

  menuWidth = '250px';

  icBeenhere = icBeenhere;
  icStars = icStars;
  icBusinessCenter = icBusinessCenter;
  icPhoneInTalk = icPhoneInTalk;
  icMail = icMail;
  icCry = icCry;
  layoutCtrl = new FormControl('boxed');
  qr;
  socketClient;
  currentSession;
  clientState;
  authenticationInProgress=false;
  actions=[
    {
      label:"Enviar Texto",
      icon:icBeenhere,
      function:()=>this.sendMessageText()
    },
    {
      label:"Enviar Multimedia",
      icon:icBeenhere,
      function:()=>this.sendMessageMedia()
    },
    {
      label:"Enviar Archivo",
      icon:icBeenhere,
      function:()=>this.sendMessageFile()
    },
    {
      label:"Enviar Sticker",
      icon:icBeenhere,
      function:()=>this.sendMessageSticker()
    }
    // {
    //   label:"Ver Estado",
    //   icon:icBeenhere,
    //   function:()=>this.getStatus()
    // }
  ]
  public devicesList;
  public currentDevice;
  public messageForm : FormGroup;
  public profileForm: FormGroup;
  public envioForm: FormGroup;
  private getStatus$Obs;
  infoStatus;
  constructor(
    private fb: FormBuilder,
    public formBuilder: FormBuilder,
    public sincronizarService: SincronizarService,
    public socketService: SocketService,
    private firebaseStorage: FirebaseStorageService,
    public sweetalertsService:SweetalertsService,
    public whatsappService: WhatsappService,
    private sanitizer: DomSanitizer
  ) { 
    this.clientState="timeout";
    this.messageForm = this.formBuilder.group({
      msg: [''],
      filename: ['nombredesdeangular'],
      to: ['+51946281558']
    });
    this.profileForm = this.formBuilder.group({
      nombre: ['', Validators.required],
      fechaHoraProgramada: ['', Validators.required],
      usuarioJefeID: ['']
    });
    this.envioForm = this.formBuilder.group({
      dispositivo: ['', Validators.required],
      destino: ['telefono', Validators.required],
      entregarAhora: [false],
      entregarAhoraValue: [''],
      mensaje: [''],
      numeroUno: [''],
      numeroDos: [''],
      numeroTres: ['']
    });
    this.socketService.events$.subscribe((event:any)=>{
      console.warn("socketService.events$: ",event);
      switch (event.event){
        case "authenticated":
          this.clientState='authenticated';
          this.currentSession=event.data;
          event.data?this.sweetalertsService.generalSuccess("Sincronizado"):this.sweetalertsService.generalError("Algo fallo.");
        break;
        case "authenticated_failure":
          this.clientState='none';
        break;
        case "authenticated_progress":
          this.clientState='none';
        break;
        case "ready":
          this.clientState='none';
        break;
        case "message_received":
          this.clientState='none';
        break;
        case "message_create":
          this.clientState='none';
        break;
        case "message_revoked_everyone":
          this.clientState='none';
        break;
        case "message_revoked_me":
          this.clientState='none';
        break;
        case "message_ack":
          this.clientState='none';
        break;
        case "message_uploaded":
          this.clientState='none';
        break;
        case "group_join":
          this.clientState='none';
        break;
        case "group_leave":
          this.clientState='none';
        break;
        case "group_update":
          this.clientState='none';
        break;
        case "qr":
          this.clientState="qrReceived";
          this.sweetalertsService.dissmissSwal();
          this.qr=event.data;
        break;
        case "qr_received":
          this.clientState='none';
        break;
        case "disconnected":
          this.qr=undefined;
          this.clientState="disconnected";
          // if(event.data=="timeout"){}//puede descronbectarse primero por socket.dissconect() y no recibir timeout reason
        break;
        case "state_changed":
          this.clientState='none';
        break;
        case "battery_changed":
          this.clientState='none';
        break;
        case "incoming_call":
          this.clientState='none';
        break;
        case "close_reason":
          this.clientState=event.data;
        break;
        case "qr_expired":
          if(event.data=="timeout"){
            this.clientState="timeout";
            this.qr=undefined;
          }//puede descronbectarse primero por socket.dissconect() y no recibir timeout reason
        break;  
      }
          
      console.info("events: ",event);
    })
  }

  sendMessageText(){
    this.whatsappService.sendMessageText(this.messageForm.getRawValue()).subscribe((resp)=>{
      console.info("resp: ",resp);
    })
  }
  sendMessageMedia(){
    this.whatsappService.sendMessageMediaUrl(this.messageForm.getRawValue()).subscribe((resp)=>{
      console.info("resp: ",resp);
    })
  }
  sendMessageFile(){
    this.whatsappService.sendMessageFileUrl(this.messageForm.getRawValue()).subscribe((resp)=>{
      console.info("resp: ",resp);
    })
  }
  sendMessageSticker(){
    this.whatsappService.sendMessageStickerUrl(this.messageForm.getRawValue()).subscribe((resp)=>{
      console.info("resp: ",resp);
    })
  }
  onSelectmultimedia(event) {
    console.log("onSelectmultimedia: ",event);
    this._adjuntosList = event;
    this.changeFile(this._adjuntosList[this._adjuntosList.length-1]);
    // if(!this.filesmultimedia.length){
    //   this.filesmultimedia = [];
    //   if(event.addedFiles[0].size<6161711){
    //     this.filesmultimedia.push(...event.addedFiles);
    //     this.firebaseStorage.uploadFile(this.filesmultimedia,"").then((publicURL:any)=>{
    //       this.adjMultimedia=new Adjunto(event,publicURL);
    //       this.agregarAdjunto(event.addedFiles[0].name, Utils.getFileExtension(event.addedFiles[0].name), event.addedFiles[0].type, publicURL);
    //       // this.multimediaData=publicURL;
    //       console.info("publicYEAH: ",publicURL);
    //     })
    //   }else{
    //     this.sweetalertsService.generalWarning("El tamaño máximo de multimedia es 6MB",2000);
    //   }
    // }
  }
  selectRowItem(index){
    console.info("selectRowItem: ",index);
    this._defaultRow=index;
  }
  onRemovemultimedia(event) {
    console.log(event);
    this.multimediaData="0";
    this.filesmultimedia.splice(this.filesmultimedia.indexOf(event), 1);
  }
  ngOnInit(): void {
    this.adjuntoActividad = this.fb.group({
      tareaID:[''],
      nombre:[''],
      extension:[''],
      type:[''],
      ruta:[''],
    });
    this.messagePattern = this.fb.group({
      textMessage: ['', Validators.required]
    });
    this.adjuntoActividad = this.fb.group({
      tareaID:[''],
      nombre:[''],
      extension:[''],
      type:[''],
      ruta:[''],
    });
    this.devicesList = Utils.toMap(dummyDevices,'id');
    this.currentDevice = this.devicesList.values().next().value;
    this.getStatus(this.currentDevice);
  }
  ngOnDestroy() {
    this.getStatus$Obs?this.getStatus$Obs.unsubscribe():null;
  }
  getStatus(currentDevice?){
    this.getStatus$Obs=this.sincronizarService.getStatus(currentDevice?currentDevice.instancia_url:this.currentDevice.instancia_url, {}).subscribe((resp)=>{
      this.infoStatus=resp;
      console.info("getStatus: ",resp );
    })
  }
  changeDevice(key){
    this.currentDevice=this.devicesList.get(key);
    console.info("changeDevice: ",this.currentDevice, key);
    this.getStatus();
  }
  private sendByTypeMessage(body){
    if(this._adjuntosList.length>0){
      this.uploadFile(this._adjuntoCurrentFile).then((success)=>{
        body.uri=success;//se agrega el path del file en firebase
        console.info("sendByTypeMessageBODY: ",body);
        if(this.adjuntoActividad.controls['extension'].value =='jpg' || this.adjuntoActividad.controls['extension'].value =='jpeg' || this.adjuntoActividad.controls['extension'].value =='png' ){
          //mensaje con imagen o solo imagen
          this.whatsappService.sendMessageMediaUrl(body).subscribe((success)=>{
            console.info("sendMessageMediaUrlSUCCESS: ",success);
          },(error)=>{
            console.info("sendMessageMediaUrlERRIR: ",error);
          });
        }
        if(this.adjuntoActividad.controls['extension'].value !='jpg' && this.adjuntoActividad.controls['extension'].value !='jpeg' && this.adjuntoActividad.controls['extension'].value !='png' && this.adjuntoActividad.controls['extension'].value != null){
          //adjunto con mensaje o solo adjunto, pero van en dos mensajes
          body.filename = "file";
          this.whatsappService.sendMessageFileUrl(body).subscribe((success)=>{
            console.info("sendMessageFileUrlSUCCESS: ",success);
          },(error)=>{
            console.info("sendMessageFileUrlERROR: ",error);
          });
          // if(this.envioForm.get('mensaje').value!=''){
          //   //mensaje del adjunto, si hay mensaje dentra aqui sino solo envia el adjunto
          // }
        }
      })
    }
    if(this._adjuntosList.length==0){
      //NO hay adjunto y solo mensaje
      this.whatsappService.sendMessageText(body).subscribe((success)=>{
        console.info("sendMessageTextSUCCESS: ",success);
      },(error)=>{
        console.info("sendMessageTextERROR: ",error);
      });
    }
    
  }
  probar(){
    let message = this.replacePatternToShow(this.envioForm.get('mensaje').value);
    console.warn("message: ",message);
    if(this.envioForm.controls.numeroUno.value!=''){
      let body={
        to:this.envioForm.controls.numeroUno.value,
        uri:this.adjuntoActividad.controls.ruta.value,
        msg:message
      }
      console.info("bodyUno: ",body);
      this.sendByTypeMessage(body);
    }
    if(this.envioForm.controls.numeroDos.value!=''){
      let body={
        to:this.envioForm.controls.numeroDos.value,
        msg:message
      }
      console.info("bodyDos: ",body);
      this.sendByTypeMessage(body);
    }
    if(this.envioForm.controls.numeroTres.value!=''){
      let body={
        to:this.envioForm.controls.numeroTres.value,
        msg:message
      }
      console.info("bodyTres: ",body);
      this.sendByTypeMessage(body);
    }
  }
  ///////PRIVATE METHODS/////////////
  private agregarAdjunto(nombre, extension, tipo, ruta){
    this.adjuntoActividad.controls['nombre'].setValue(nombre);
    this.adjuntoActividad.controls['extension'].setValue(extension);
    this.adjuntoActividad.controls['type'].setValue(tipo);
    this.adjuntoActividad.controls['ruta'].setValue(ruta);
    console.info("agregarAdjunto: ", JSON.stringify(this.adjuntoActividad.getRawValue()));
    return JSON.stringify(this.adjuntoActividad.getRawValue());
  }
  changeFile(item){
    console.info("changeFile: ",item);
    this._adjuntoCurrentFile=item.file;
    this._adjuntoImagen=item.preview;
    this.agregarAdjunto(item.name,item.fileExtension,item.tipo,item.preview);
  }
//////////////////FUNCIONES EXTRAS////////////////
replacePatternToShow(rawMessage){
  let tempMessage=rawMessage;
  if(this.excelHeaders){
    for(let y=0;y < this.excelHeaders.length;y++){
      let mapp = new Map();
      mapp.set(this.excelHeaders[y],this.messagesArray[this._defaultRow][y+2]);
      // console.info("excelHeaders: ",mapp);
      for(let z=2;z<this.messagesArray[this._defaultRow].length;z++){
          if(rawMessage.indexOf('{{'+this.excelHeaders[y]+'}}')!= -1){ 
            tempMessage = tempMessage.replaceAll('{{'+this.excelHeaders[y]+'}}', mapp.get(this.excelHeaders[y]));
          }
      }
    }
  }
  return tempMessage;
}
replacePattern(rawMessage,index){
  let tempMessage=rawMessage;
  if(this.excelHeaders){
    for(let y=0;y < this.excelHeaders.length;y++){
      let mapp = new Map();
      mapp.set(this.excelHeaders[y],this.messagesArray[index][y+2]);
      // console.info("excelHeaders: ",mapp);
      for(let z=2;z<this.messagesArray[index].length;z++){
          if(rawMessage.indexOf('{{'+this.excelHeaders[y]+'}}')!= -1){ 
            tempMessage = tempMessage.replaceAll('{{'+this.excelHeaders[y]+'}}', mapp.get(this.excelHeaders[y]));
          }
      }
    }
  }
  return tempMessage;
}
public concatParamInMessage(param){
  this.envioForm.get('mensaje').setValue(
    this.envioForm.get('mensaje').value+"{{"+param+"}} "
  );
  document.getElementById("textAreaMessage").focus();
  console.info("param: ",param);
  console.info("button param: ",this.envioForm.get('mensaje').value);
}
onSelectcampana(event) {
  console.warn("onSelectcampana: ",event);
  // this.eventsService.showLoading();
  this.filescampana = [];
  if(event.addedFiles[0].size<6161711){
    this.filescampana.push(...event.addedFiles);
    this.firebaseStorage.uploadFile(this.filescampana,"").then((publicURL:any)=>{
      this.adjCampania=new Adjunto(event,publicURL);
    })
  }else{
    this.sweetalertsService.generalWarning("El tamaño máximo de multimedia es 6MB",2000);
  }
  setTimeout(x=>{
    this.validateExcel(this.filescampana[0]); 
  },2000);
  this.fileContainer.delete("archivo");
  this.fileContainer.append("archivo", this.filescampana[0], this.filescampana[0].name);
  // this.uploadFile(this.fileContainer);
}
uploadFile(file) {
  return new Promise((resolve,reject)=>{
    console.warn("uploadFile: ",file);
    // this.eventsService.showLoading();
    if(file.size<6161711){
      this.firebaseStorage.uploadFile([file],"").then((publicURL:any)=>{
        // this.adjCampania=new Adjunto(file,publicURL);//this.adjCampania.ruta es la ruta publica depsues de subir
        resolve(publicURL);
      })
    }else{
      this.sweetalertsService.generalWarning("El tamaño máximo de multimedia es 6MB",2000);
      reject("fail upload");
    }
    // this.fileContainer.delete("archivo");
    // this.fileContainer.append("archivo", this.filescampana[0], this.filescampana[0].name);
    // this.uploadFile(this.fileContainer);
  })
}
uploadBase64toFirebase(base64data){
  const blob = new Blob([JSON.stringify(base64data, null, 2)], {type: 'application/json'});
  // console.info("BLOB: ",blob);
  this.firebaseStorage.uploadBlob(blob,'temporal/'+Utils.filePathFirebase()).then((resp)=>{
    console.warn("json url file: ",resp);
  });
}
onRemovecampana(event) {
  console.log(event);
  this.filescampana.splice(this.filescampana.indexOf(event), 1);
  this.excelValidatorErrors=[];
  this.excelHeaders = [];
  this.messagePattern.controls['textMessage'].setValue("");
}
public refreshVariables(){
  this.senderMessageCount=0;
  this.senderRowsCount=this.messagesArray?this.messagesArray.length:0;
  this.metricSuccessMessages=0;
  this.metricFailMessages=0;
  this.metricSuccessSendedMessages=0;
  this.metricSuccessEnqueueMessages=0;
}
private validateExcel(file){
  this.refreshVariables();
  this.excelValidatorErrors=[];
  this.getHeadersSchema(file).then((rows:any[])=>{
      for (var _i = 1; _i < rows.length; _i++) {
        var fila = rows[_i];
        if(!Global.validatePhone(fila[0])){
          console.error("error telefono origen: ", "fila: ", _i);
          this.excelValidatorErrors.push({fila:_i,error:"telefono origen: "});
        }
        if(!Global.validatePhone(fila[1])){
          console.error("error telefono destino: ", "fila: ", _i);
          this.excelValidatorErrors.push({fila:_i,error:"telefono destino: "});
        }
      }
    // this.eventsService.hideLoading();
  },(reject)=>{
    // this.eventsService.hideLoading();
    this.sweetalertsService.generalWarning(reject);
  })
}
  private getHeadersSchema(file){
    return new Promise((resolve,reject)=>{
      let fileReader = new FileReader();    
      fileReader.readAsArrayBuffer(file);     
      fileReader.onload = (e) => {    
          this.arrayBuffer = fileReader.result;    
          var data = new Uint8Array(this.arrayBuffer);    
          var arr = new Array();    
          for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);    
          var bstr = arr.join("");
          ////LEE LAS DOS PRIMERAS FILAS
          var workbook_headers = XLSX.read(bstr, {type:"binary",sheetRows: 2});    
          var first_sheet_name_headers = workbook_headers.SheetNames[0];    
          var worksheet_headers = workbook_headers.Sheets[first_sheet_name_headers];
          let _headers=XLSX.utils.sheet_to_json(worksheet_headers,{raw:true,header: 1});
          if(!(_headers[0][0]==_headers[1][0])){
            this.firstRow=_headers[0];
            this.fromTo=this.firstRow.slice(0,2);
            this.excelHeaders=this.firstRow.slice(2);
            var workbook = XLSX.read(bstr, {type:"binary"});    
            var first_sheet_name = workbook.SheetNames[0];    
            var worksheet = workbook.Sheets[first_sheet_name];    
            this.messagesArray = XLSX.utils.sheet_to_json(worksheet,{raw:true,header: 1,range:1,blankrows:false});     
            this.senderRowsCount=this.messagesArray.length;
            console.error("XLS TO JSON######: ",this.messagesArray);    
            resolve (this.messagesArray);
          }else{
            console.warn("El archivo no tiene cabeceras");
            reject("El archivo no tiene cabeceras");
          }
      }    
    })
  }
  getConvertedMessages(){
    let temp_array=[];
    if(this._adjuntosList.length>0){
      this.uploadFile(this._adjuntoCurrentFile).then((success)=>{
        this.messagesArray.forEach((item, index) => {
            let temp_item={to:undefined,from:undefined,uri:undefined,message:undefined,type:undefined,filename:undefined,entregar_ahora:undefined,fecha_hora:undefined};
            temp_item.uri=success;//se agrega el path del file en firebase
            // console.info("sendByTypeMessageBODY: ",item.uri);
            if(this.adjuntoActividad.controls['extension'].value =='jpg' || this.adjuntoActividad.controls['extension'].value =='jpeg' || this.adjuntoActividad.controls['extension'].value =='png' ){
              //mensaje con imagen o solo imagen
              temp_item.type="sendMessageMediaUrl"
            }
            if(this.adjuntoActividad.controls['extension'].value !='jpg' && this.adjuntoActividad.controls['extension'].value !='jpeg' && this.adjuntoActividad.controls['extension'].value !='png' && this.adjuntoActividad.controls['extension'].value != null){
              //adjunto con mensaje o solo adjunto, pero van en dos mensajes
              temp_item.filename="file";
              temp_item.type="sendMessageFileUrl"
              // if(this.envioForm.get('mensaje').value!=''){
              //   //mensaje del adjunto, si hay mensaje dentra aqui sino solo envia el adjunto
              // }
            }
            if(this.envioForm.controls.entregarAhora.value!=true){
              temp_item.entregar_ahora=this.envioForm.controls.entregarAhora.value;
              temp_item.fecha_hora=this.envioForm.controls.entregarAhoraValue.value;
            }else{
              temp_item.entregar_ahora=this.envioForm.controls.entregarAhora.value;
              temp_item.fecha_hora="0";
            }
            temp_item.message=this.replacePattern(this.envioForm.get('mensaje').value,index);
            temp_item.from=item[0];
            temp_item.to=item[1];
            console.warn("temp_item: ",temp_item);
            temp_array.push(temp_item);
        });
        var theJSON = JSON.stringify(temp_array);
        console.info("theJSON uri: ",theJSON);
        this.uploadBase64toFirebase(theJSON);
      });
    }
    if(this._adjuntosList.length==0){
      this.messagesArray.forEach((item, index) => {
        let temp_item={to:undefined,from:undefined,uri:undefined,message:undefined,type:undefined,filename:undefined,entregar_ahora:undefined,fecha_hora:undefined};
        //NO hay adjunto y solo mensaje
        temp_item.type="sendMessageText";
        if(this.envioForm.controls.entregarAhora.value!=true){
          temp_item.entregar_ahora=this.envioForm.controls.entregarAhora.value;
          temp_item.fecha_hora=this.envioForm.controls.entregarAhoraValue.value;
        }else{
          temp_item.entregar_ahora=this.envioForm.controls.entregarAhora.value;
          temp_item.fecha_hora="0";
        }
        temp_item.message=this.replacePattern(this.envioForm.get('mensaje').value,index);
        temp_item.from=item[0];
        temp_item.to=item[1];
        //sendMessageText
        temp_array.push(temp_item);
      });
      var theJSON = JSON.stringify(temp_array);
      console.info("theJSON uri: ",theJSON);
      this.uploadBase64toFirebase(theJSON);
    }
  }
}
