import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import icVisibility from '@iconify/icons-ic/twotone-visibility';
import icVisibilityOff from '@iconify/icons-ic/twotone-visibility-off';
import { fadeInUp400ms } from '../../../../../@vex/animations/fade-in-up.animation';
import { ApiService } from 'src/app/services/api.service';
import { SweetalertsService } from 'src/app/services/sweetalerts.service';

@Component({
  selector: 'vex-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    fadeInUp400ms
  ]
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  inputType = 'password';
  visible = false;

  icVisibility = icVisibility;
  icVisibilityOff = icVisibilityOff;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private snackbar: MatSnackBar,
    private apiService: ApiService,
    public sweetalertsService: SweetalertsService
  ) {}

  ngOnInit() {
    this.form = this.fb.group({
      correo: ['alexcampos@java.com.pe', Validators.required],
      clave: ['123456', Validators.required]
    });
  }

  send() {
    this.router.navigate(['/']);
    this.snackbar.open('Lucky you! Looks like you didn\'t need a password or usuario address! For a real application we provide validators to prevent this. ;)', 'LOL THANKS', {
      duration: 10000
    });
  }
  login(){
    let body=this.form.getRawValue();
    console.info("login",body);
    this.apiService.login(body).subscribe((resp)=>{
      console.info("loginSUCCESS: ",resp);
      if(resp['status']=='OK'){
        // this.presentModalResumenPage(resp['result']);
        this.router.navigateByUrl('/apps/sincronizar');
      }else{
        resp['info']==""?null:this.sweetalertsService.generalError(resp['info']);
      }
    },(error)=>{
      console.error("loginERROR: ",error);
      this.sweetalertsService.generalError(error.info);
    })
  }

  toggleVisibility() {
    if (this.visible) {
      this.inputType = 'password';
      this.visible = false;
      this.cd.markForCheck();
    } else {
      this.inputType = 'text';
      this.visible = true;
      this.cd.markForCheck();
    }
  }
}
