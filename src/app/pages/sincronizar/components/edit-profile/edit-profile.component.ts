import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import icStar from '@iconify/icons-ic/twotone-star';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icClose from '@iconify/icons-ic/twotone-close';
import icPrint from '@iconify/icons-ic/twotone-print';
import icDownload from '@iconify/icons-ic/twotone-cloud-download';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icBusiness from '@iconify/icons-ic/twotone-business';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icEmail from '@iconify/icons-ic/twotone-mail';
import icPerson from '@iconify/icons-ic/twotone-person';
import icStarBorder from '@iconify/icons-ic/twotone-star-border';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ContactsEditComponent } from 'src/app/pages/apps/contacts/components/contacts-edit/contacts-edit.component';
import { Contact } from 'src/app/pages/apps/contacts/interfaces/contact.interface';
import { WhatsappService } from 'src/app/services/whatsapp.service';
import { SweetalertsService } from 'src/app/services/sweetalerts.service';

export let contactIdCounter = 50;

@Component({
  selector: 'vex-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {

  contact: Contact;

  icStar = icStar;
  icStarBorder = icStarBorder;
  icMoreVert = icMoreVert;
  icClose = icClose;

  icPrint = icPrint;
  icDownload = icDownload;
  icDelete = icDelete;

  icBusiness = icBusiness;
  icPerson = icPerson;
  icEmail = icEmail;
  icPhone = icPhone;
  public profileForm : FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) private contactId: Contact['id'],
    private dialogRef: MatDialogRef<ContactsEditComponent>,
    private fb: FormBuilder,
    public formBuilder: FormBuilder,
    public whatsappService: WhatsappService,
    public sweetalertsService: SweetalertsService
  ) { 
    this.profileForm = this.formBuilder.group({
      displayName: [''],
      status: ['']
    });
  }

  ngOnInit() {

  }

  save() {
    let body=this.profileForm.getRawValue();
    this.whatsappService.setStatus(body).subscribe(()=>{
      this.sweetalertsService.generalSuccess("Estado Actualizado");
    },(error)=>{
      this.sweetalertsService.generalError("Error actualizando estado");
    })
    this.whatsappService.setDisplayName(body).subscribe(()=>{
      this.sweetalertsService.generalSuccess("Nombre Actualizado");
    },(error)=>{
      this.sweetalertsService.generalError("Error actualizando nombre");
    })
    this.dialogRef.close();
  }
}
