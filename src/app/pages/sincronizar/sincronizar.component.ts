import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { fadeInUp400ms } from 'src/@vex/animations/fade-in-up.animation';
import { stagger60ms } from 'src/@vex/animations/stagger.animation';
import { SincronizarService } from 'src/app/services/sincronizar.service';
import { SocketService } from 'src/app/services/socket.service';
import { SweetalertsService } from 'src/app/services/sweetalerts.service';
import { WhatsappService } from 'src/app/services/whatsapp.service';
// import { QRCode } from
import icBeenhere from '@iconify/icons-ic/twotone-beenhere';
import icStars from '@iconify/icons-ic/twotone-stars';
import icBusinessCenter from '@iconify/icons-ic/twotone-business-center';
import icPhoneInTalk from '@iconify/icons-ic/twotone-phone-in-talk';
import icMail from '@iconify/icons-ic/twotone-mail';
import icCry from '@iconify/icons-fa-solid/sad-cry';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import * as Global from '../../globals';

@Component({
  selector: 'vex-sincronizar',
  templateUrl: './sincronizar.component.html',
  styleUrls: ['./sincronizar.component.scss'],
  animations: [
    stagger60ms,
    fadeInUp400ms
  ]
})
export class SincronizarComponent implements OnInit {
  menuWidth = '250px';

  icBeenhere = icBeenhere;
  icStars = icStars;
  icBusinessCenter = icBusinessCenter;
  icPhoneInTalk = icPhoneInTalk;
  icMail = icMail;
  icCry = icCry;
  layoutCtrl = new FormControl('boxed');
  qr;
  socketClient;
  currentSession;
  clientState;
  authenticationInProgress=false;
  actions=[
    {
      label:"Sicronizar",
      icon:icBeenhere,
      function:()=>this.sincronize()
    },
    {
      label:"Restaurar Sesion",
      icon:icBeenhere
    },
    {
      label:"Forzar Sicronizacion",
      icon:icBeenhere,
      function:"sincronize"
    },
    {
      label:"Resetear",
      icon:icBeenhere,
      function:()=>this.logout()
    },
    {
      label:"Desconectar",
      icon:icBeenhere,
      function:()=>this.logout()
    },
    {
      label:"Ver Estado",
      icon:icBeenhere,
      function:()=>this.getStatus()
    }
  ]
  infoStatus;
  public messageForm : FormGroup;
  public whatsappInstance : FormGroup;
  public whatsappSaveInstance : FormGroup;
  public whatsappTokenInstance : FormGroup;
  constructor(
    public formBuilder: FormBuilder,
    public sincronizarService: SincronizarService,
    public socketService: SocketService,
    public sweetalertsService:SweetalertsService,
    public whatsappService: WhatsappService,
    public usuariosService: UsuariosService,
    private matDialog: MatDialog,
  ) { 
    // this.sincronizarService.getQr().subscribe((resp)=>{
    //   this.qr=resp.qr;
    //   console.info("getQr: ",resp);
    // })
    this.clientState="none";
    this.messageForm = this.formBuilder.group({
      msg: [''],
      filename: ['nombredesdeangular'],
      to: ['+51946281558']
    });
    this.whatsappInstance = this.formBuilder.group({
      instancia: [Global.local_url],
      token: [Global.token_url]
    });
    this.whatsappSaveInstance = this.formBuilder.group({
      instancia: [''],
      token: [''],
      fechaHora: [''],
      hash: ['']
    });
    this.whatsappTokenInstance = this.formBuilder.group({
      WABrowserId: [''],
      WASecretBundle: [''],
      WAToken1: [''],
      WAToken2: [''],
    });
    this.socketService.events$.subscribe((event:any)=>{
      console.warn("socketService.events$: ",event);
      switch (event.event){
        case "auth_failure":
          this.clientState='auth_failure';
        break;
        case "authenticated":
          this.clientState='authenticated';
          this.currentSession=event.data;
          console.info("currentSession: ",this.currentSession);
          event.data?this.sweetalertsService.generalSuccess("Sincronizado"):this.sweetalertsService.generalError("Algo fallo.");
        break;
        case "change_battery":
          this.clientState='change_battery';
        break;
        case "change_state":
          this.clientState='change_state';
        break;
        case "disconnected":
          this.clientState='disconnected';
        break;
        case "group_join":
          this.clientState='group_join';
        break;
        case "group_leave":
          this.clientState='group_leave';
        break;
        case "group_update":
          this.clientState='group_update';
        break;
        case "incoming_call":
          this.clientState='incoming_call';
        break;
        case "media_uploaded":
          this.clientState='media_uploaded';
        break;
        case "message":
          this.clientState='message';
        break;
        case "message_ack":
          this.clientState='message_ack';
        break;
        case "message_create":
          this.clientState='message_create';
        break;
        case "message_revoke_everyone":
          this.clientState='message_revoke_everyone';
        break;
        case "message_revoke_me":
          this.clientState='message_revoke_me';
        break;
        case "qr":
          this.clientState="qrReceived";
          this.sweetalertsService.dissmissSwal();
          this.qr=event.data;
        break;
        case "ready":
          this.clientState='ready';
        break;
        ///EXTRA
        case "disconnected":
          this.qr=undefined;
          this.clientState="disconnected";
          // if(event.data=="timeout"){}//puede descronbectarse primero por socket.dissconect() y no recibir timeout reason
        break;
        case "close_reason":
          this.clientState=event.data;
        break;
        case "qr_expired":
          if(event.data=="timeout"){
            this.clientState="timeout";
            this.qr=undefined;
          }//puede descronbectarse primero por socket.dissconect() y no recibir timeout reason
        break;  
      }
          
      console.info("events: ",event);
    })
    this.socketService.authenticationInProgress$.subscribe((authenticationInProgress:any)=>{
      console.info("authenticationInProgress: ",authenticationInProgress);
      this.sweetalertsService.loading();
      this.authenticationInProgress=authenticationInProgress;
    })
  }
  sincronize(){
    // this.clientState="sincronize";
    // this.sweetalertsService.loading();
    // this.sincronizarService.sincronize().subscribe((resp)=>{
    //   this.socketClient=this.socketService.init();
    // },(error)=>{
    //   this.sweetalertsService.generalError("Fallo obteniendo QR");
    // })
    console.info("Sincornize necesita la url del dominio");
  }
  whatsappGetInstance(){
    let body=this.whatsappInstance.getRawValue();
    console.info("whatsappSaveSessionBODY: ",body);
    this.usuariosService.whatsappClientGetInstance(body).subscribe((resp)=>{
      if(resp['status']=="SUCCESS"){
        // this.sweetalertsService.generalSuccess("Instancia Corriendo");
        console.info("Instancia Corriendo");
      }else if(resp['status']=="WARN"){
        // this.sweetalertsService.generalSuccess("Cliente no iniciado");
        console.info("Cliente no iniciado");
      }
    });
  }
  logout(){
    // this.sincronizarService.logout().subscribe((resp)=>{
    //   console.info("logout: ",resp );
    //   this.sweetalertsService.generalSuccess("Sesion cerrada");
    // })
    // this.socketService.end(this.socketClient);
    console.info("logout necesita la url del dominio");
  }
  socketInit(){
    this.socketClient=this.socketService.init();
  }
  getStatus(){
    this.sincronizarService.getStatus(this.currentSession).subscribe((resp)=>{
      this.infoStatus=resp;
      console.info("getStatus: ",resp );
    })
  }
  currentClients(){
    this.whatsappService.currentClients(this.currentSession).subscribe((resp)=>{
      console.info("currentClients: ",resp);
    })
  }
  sendMessageText(){
    this.whatsappService.sendMessageText(this.messageForm.getRawValue()).subscribe((resp)=>{
      console.info("resp: ",resp);
    })
  }
  sendMessageMediaUrl(){
    this.whatsappService.sendMessageMediaUrl(this.messageForm.getRawValue()).subscribe((resp)=>{
      console.info("resp: ",resp);
    })
  }
  sendMessageFileUrl(){
    this.whatsappService.sendMessageFileUrl(this.messageForm.getRawValue()).subscribe((resp)=>{
      console.info("resp: ",resp);
    })
  }
  sendMessageStickerUrl(){
    this.whatsappService.sendMessageStickerUrl(this.messageForm.getRawValue()).subscribe((resp)=>{
      console.info("resp: ",resp);
    })
  }
  ngOnInit(): void {
    console.info("ngOnInit: ");
    this.getStatus();
  }




  ///EXTRA EVENTS///
  editName(){
    console.info("edit name");
  }
  editState(){

  }
  openEdit() {
    this.matDialog.open(EditProfileComponent, {
      data: null,
      width: '600px'
    });
  }
}
