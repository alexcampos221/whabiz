import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { fadeInUp400ms } from 'src/@vex/animations/fade-in-up.animation';
import { stagger60ms } from 'src/@vex/animations/stagger.animation';
import { SincronizarService } from 'src/app/services/sincronizar.service';
import { SocketService } from 'src/app/services/socket.service';
import { SweetalertsService } from 'src/app/services/sweetalerts.service';
import { WhatsappService } from 'src/app/services/whatsapp.service';
// import { QRCode } from
import icBeenhere from '@iconify/icons-ic/twotone-beenhere';
import icStars from '@iconify/icons-ic/twotone-stars';
import icBusinessCenter from '@iconify/icons-ic/twotone-business-center';
import icPhoneInTalk from '@iconify/icons-ic/twotone-phone-in-talk';
import icMail from '@iconify/icons-ic/twotone-mail';
import icCry from '@iconify/icons-fa-solid/sad-cry';
import { FirebaseStorageService } from 'src/app/services/firebase-storage.service';
import { Utils } from 'src/app/utils';
import { Adjunto } from 'src/app/models/adjunto.models';
@Component({
  selector: 'vex-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
  animations: [
    stagger60ms,
    fadeInUp400ms
  ]
})
export class TestComponent implements OnInit {
  ///VEX-FILE-UPLOAD
  multimediaData="0";
  filesprueba: File[] = [];filescampana: File[] = [];filesmultimedia: File[] = [];
  adjMultimedia;
  adjuntoActividad: FormGroup;
  //
  menuWidth = '250px';

  icBeenhere = icBeenhere;
  icStars = icStars;
  icBusinessCenter = icBusinessCenter;
  icPhoneInTalk = icPhoneInTalk;
  icMail = icMail;
  icCry = icCry;
  layoutCtrl = new FormControl('boxed');
  qr;
  socketClient;
  currentSession;
  clientState;
  authenticationInProgress=false;
  actions=[
    {
      label:"Enviar Texto",
      icon:icBeenhere,
      function:()=>this.sendMessageText()
    },
    {
      label:"Enviar Multimedia",
      icon:icBeenhere,
      function:()=>this.sendMessageMedia()
    },
    {
      label:"Enviar Archivo",
      icon:icBeenhere,
      function:()=>this.sendMessageFile()
    },
    {
      label:"Enviar Sticker",
      icon:icBeenhere,
      function:()=>this.sendMessageSticker()
    }
    // {
    //   label:"Ver Estado",
    //   icon:icBeenhere,
    //   function:()=>this.getStatus()
    // }
  ]

  public messageForm : FormGroup;
  public profileForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    public formBuilder: FormBuilder,
    public sincronizarService: SincronizarService,
    public socketService: SocketService,
    private firebaseStorage: FirebaseStorageService,
    public sweetalertsService:SweetalertsService,
    public whatsappService: WhatsappService
  ) { 
    this.clientState="timeout";
    this.messageForm = this.formBuilder.group({
      msg: [''],
      filename: ['nombredesdeangular'],
      to: ['+51946281558']
    });
    this.profileForm = this.formBuilder.group({
      nombre: ['', Validators.required],
      fechaHoraProgramada: ['', Validators.required],
      usuarioJefeID: ['']
    });
    this.socketService.events$.subscribe((event:any)=>{
      console.warn("socketService.events$: ",event);
      switch (event.event){
        case "authenticated":
          this.clientState='authenticated';
          this.currentSession=event.data;
          event.data?this.sweetalertsService.generalSuccess("Sincronizado"):this.sweetalertsService.generalError("Algo fallo.");
        break;
        case "authenticated_failure":
          this.clientState='none';
        break;
        case "authenticated_progress":
          this.clientState='none';
        break;
        case "ready":
          this.clientState='none';
        break;
        case "message_received":
          this.clientState='none';
        break;
        case "message_create":
          this.clientState='none';
        break;
        case "message_revoked_everyone":
          this.clientState='none';
        break;
        case "message_revoked_me":
          this.clientState='none';
        break;
        case "message_ack":
          this.clientState='none';
        break;
        case "message_uploaded":
          this.clientState='none';
        break;
        case "group_join":
          this.clientState='none';
        break;
        case "group_leave":
          this.clientState='none';
        break;
        case "group_update":
          this.clientState='none';
        break;
        case "qr":
          this.clientState="qrReceived";
          this.sweetalertsService.dissmissSwal();
          this.qr=event.data;
        break;
        case "qr_received":
          this.clientState='none';
        break;
        case "disconnected":
          this.qr=undefined;
          this.clientState="disconnected";
          // if(event.data=="timeout"){}//puede descronbectarse primero por socket.dissconect() y no recibir timeout reason
        break;
        case "state_changed":
          this.clientState='none';
        break;
        case "battery_changed":
          this.clientState='none';
        break;
        case "incoming_call":
          this.clientState='none';
        break;
        case "close_reason":
          this.clientState=event.data;
        break;
        case "qr_expired":
          if(event.data=="timeout"){
            this.clientState="timeout";
            this.qr=undefined;
          }//puede descronbectarse primero por socket.dissconect() y no recibir timeout reason
        break;  
      }
          
      console.info("events: ",event);
    })
  }

  sendMessageText(){
    this.whatsappService.sendMessageText(this.messageForm.getRawValue()).subscribe((resp)=>{
      console.info("resp: ",resp);
    })
  }
  sendMessageMedia(){
    this.whatsappService.sendMessageMediaUrl(this.messageForm.getRawValue()).subscribe((resp)=>{
      console.info("resp: ",resp);
    })
  }
  sendMessageFile(){
    this.whatsappService.sendMessageFileUrl(this.messageForm.getRawValue()).subscribe((resp)=>{
      console.info("resp: ",resp);
    })
  }
  sendMessageSticker(){
    this.whatsappService.sendMessageStickerUrl(this.messageForm.getRawValue()).subscribe((resp)=>{
      console.info("resp: ",resp);
    })
  }
  onSelectmultimedia(event) {
    console.log("onSelectmultimedia: ",event);
    if(!this.filesmultimedia.length){
      this.filesmultimedia = [];
      if(event.addedFiles[0].size<6161711){
        this.filesmultimedia.push(...event.addedFiles);
        this.firebaseStorage.uploadFile(this.filesmultimedia,"").then((publicURL:any)=>{
          this.adjMultimedia=new Adjunto(event,publicURL);
          this.agregarAdjunto(event.addedFiles[0].name, Utils.getFileExtension(event.addedFiles[0].name), event.addedFiles[0].type, publicURL);
          // this.multimediaData=publicURL;
          console.info("publicYEAH: ",publicURL);
        })
      }else{
        this.sweetalertsService.generalWarning("El tamaño máximo de multimedia es 6MB",2000);
      }
    }
  }
  onRemovemultimedia(event) {
    console.log(event);
    this.multimediaData="0";
    this.filesmultimedia.splice(this.filesmultimedia.indexOf(event), 1);
  }
  ngOnInit(): void {
    this.adjuntoActividad = this.fb.group({
      tareaID:[''],
      nombre:[''],
      extension:[''],
      type:[''],
      ruta:[''],
    });
  }
  save(){
    
  }
  ///////PRIVATE METHODS/////////////
  private agregarAdjunto(nombre, extension, tipo, ruta){
    this.adjuntoActividad.controls['nombre'].setValue(nombre);
    this.adjuntoActividad.controls['extension'].setValue(extension);
    this.adjuntoActividad.controls['type'].setValue(tipo);
    this.adjuntoActividad.controls['ruta'].setValue(ruta);
    console.info("agregarAdjunto: ", JSON.stringify(this.adjuntoActividad.getRawValue()));
    return JSON.stringify(this.adjuntoActividad.getRawValue());
  }
}
