import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { Observable, of, ReplaySubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import print from '@iconify/icons-ic/print'
import { SelectionModel } from '@angular/cdk/collections';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icFolder from '@iconify/icons-ic/twotone-folder';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions } from '@angular/material/form-field';
import { FormControl } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { MatSelectChange } from '@angular/material/select';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icMail from '@iconify/icons-ic/twotone-mail';
import icMap from '@iconify/icons-ic/twotone-map';
import { fadeInUp400ms } from 'src/@vex/animations/fade-in-up.animation';
import { stagger40ms } from 'src/@vex/animations/stagger.animation';
import { TableColumn } from 'src/@vex/interfaces/table-column.interface';
import { aioTableLabels, aioTableData } from 'src/static-data/aio-table-data';
import { CustomerCreateUpdateComponent } from '../apps/aio-table/customer-create-update/customer-create-update.component';
import { Customer } from '../apps/aio-table/interfaces/customer.model';
import { TestService } from 'src/app/services/test.service';
import { AnonymousSubject } from 'rxjs/internal/Subject';
import { LoadingService } from 'src/app/services/loading.service';
import { SweetalertsService } from 'src/app/services/sweetalerts.service';
import { ExportExcelService } from 'src/app/services/export-excel.service';


@Component({
  selector: 'vex-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class UserComponent implements OnInit {

  layoutCtrl = new FormControl('boxed');

  /**
   * Simulating a service with HTTP that returns Observables
   * You probably want to remove this and do all requests in a service with HTTP
   */
  subject$: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
  data$: Observable<any[]> = this.subject$.asObservable();
  list;

  @Input()
  columns: TableColumn<any>[] = [
    // { label: 'Checkbox', property: 'checkbox', type: 'checkbox', visible: true },
    // { label: 'Image', property: 'image', type: 'image', visible: true },
    // { label: 'Name', property: 'name', type: 'text', visible: true, cssClasses: ['font-medium'] },
    // { label: 'First Name', property: 'firstName', type: 'text', visible: false },
    // { label: 'Last Name', property: 'lastName', type: 'text', visible: false },
    // { label: 'Contact', property: 'contact', type: 'button', visible: true },
    // { label: 'Address', property: 'address', type: 'text', visible: true, cssClasses: ['text-secondary', 'font-medium'] },
    // { label: 'Street', property: 'street', type: 'text', visible: false, cssClasses: ['text-secondary', 'font-medium'] },
    // { label: 'Zipcode', property: 'zipcode', type: 'text', visible: false, cssClasses: ['text-secondary', 'font-medium'] },
    // { label: 'City', property: 'city', type: 'text', visible: false, cssClasses: ['text-secondary', 'font-medium'] },
    // { label: 'Phone', property: 'phoneNumber', type: 'text', visible: true, cssClasses: ['text-secondary', 'font-medium'] },
    // { label: 'Labels', property: 'labels', type: 'button', visible: true },
    // { label: 'Actions', property: 'actions', type: 'button', visible: true }
    { label: 'Usuario', property: 'usu_nombre', type: 'text', visible: true },
    { label: 'Local', property: 'loc_nombre', type: 'text', visible: true },
    { label: 'Documento', property: 'doc_numero', type: 'text', visible: true },
    { label: 'Hora Ingreso', property: 'par_hora_ingreso_l', type: 'text', visible: true },
    { label: 'Hora Pago', property: 'par_hora_pago_l', type: 'text', visible: true },
    { label: 'Placa', property: 'par_placa', type: 'text', visible: true }

  ];
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<any> | null;
  selection = new SelectionModel<any>(true, []);
  searchCtrl = new FormControl();

  labels = aioTableLabels;

  icPhone = icPhone;
  icMail = icMail;
  icMap = icMap;
  icEdit = icEdit;
  icSearch = icSearch;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  print = print;
  icMoreHoriz = icMoreHoriz;
  icFolder = icFolder;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    public testService: TestService,
    public loadingService:LoadingService,
    public sweetalertsService: SweetalertsService,
    public exportExcelService: ExportExcelService
  ) {
  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  /**
   * 
   OTHER METHODS
   */
   listarComprobantes(){
     let body={
      localID: 1000010
     }
     console.info("listarComprobantesBODY: ",body);
     this.loadingService.show();
     this.testService.listarComprobantes(body).subscribe((resp)=>{
      this.loadingService.hide();
      this.subject$.next(resp);
       console.info("listarComprobantesSUCCESS: ",resp);
     })
   }
   showLoading(){
    console.info("show loading");
    this.sweetalertsService.loading();
   }
   exportToExcel(){
    let dataToExport = this.list;
    console.info("VALUES OBJECT: ",Object.keys(this.list[0]));
    // { label: 'Usuario', property: 'usu_nombre', type: 'text', visible: true },
    // { label: 'Local', property: 'loc_nombre', type: 'text', visible: true },
    // { label: 'Documento', property: 'doc_numero', type: 'text', visible: true },
    // { label: 'Hora Ingreso', property: 'par_hora_ingreso_l', type: 'text', visible: true },
    // { label: 'Hora Pago', property: 'par_hora_pago_l', type: 'text', visible: true },
    // { label: 'Placa', property: 'par_placa', type: 'text', visible: true }
    // let dataForExcel=[];
    // console.info("filteredUsersList: ",this.filteredUsersList);
    // for(let row of dataToExport) {
    //   dataForExcel.push(Object.values(row));
    // }
    let mapHeaders=new Map([
      ["Usuario Id","usu_nombre"],
      ["Local","loc_nombre"],
      ["Documento","doc_numero"],
      ["Hora Ingreso","par_hora_ingreso_l"],
      ["Hora Pago","par_hora_pago_l"],
      ["Placa","par_placa"]
    ]);
    let mapHeadersShow=[
      "Usuario Id",
      "Local",
      "Documento",
      "Hora Ingreso",
      "Hora Pago",
      "Placa"
    ];
    let reportData = {
      title: "Reporte Usuarios",
      data: dataToExport,
      mapHeaders:mapHeaders,
      mapHeadersShow:mapHeadersShow,
      filtro:""
    }
    this.exportExcelService.exportExcelBasic(reportData);
  }

  /**
   * Example on how to get data and pass it to the table - usually you would want a dedicated service with a HTTP request for this
   * We are simulating this request here.
   */
  // getData() {
  //   return of(aioTableData.map(customer => new Customer(customer)));
  // }

  ngOnInit() {
    // this.getData().subscribe(list => {
    //   this.subject$.next(list);
    // });
    this.listarComprobantes();

    this.dataSource = new MatTableDataSource();

    this.data$.pipe(
      filter<Customer[]>(Boolean)
    ).subscribe(list => {
      this.list = list;
      this.dataSource.data = list;
    });

    this.searchCtrl.valueChanges.pipe(
      untilDestroyed(this)
    ).subscribe(value => this.onFilterChange(value));
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  createCustomer() {
    this.dialog.open(CustomerCreateUpdateComponent).afterClosed().subscribe((customer: Customer) => {
      /**
       * Customer is the updated customer (if the user pressed Save - otherwise it's null)
       */
      if (customer) {
        /**
         * Here we are updating our local array.
         * You would probably make an HTTP request here.
         */
        this.list.unshift(new Customer(customer));
        this.subject$.next(this.list);
      }
    });
  }

  updateCustomer(customer: Customer) {
    this.dialog.open(CustomerCreateUpdateComponent, {
      data: customer
    }).afterClosed().subscribe(updatedCustomer => {
      /**
       * Customer is the updated customer (if the user pressed Save - otherwise it's null)
       */
      if (updatedCustomer) {
        /**
         * Here we are updating our local array.
         * You would probably make an HTTP request here.
         */
        const index = this.list.findIndex((existingCustomer) => existingCustomer.id === updatedCustomer.id);
        this.list[index] = new Customer(updatedCustomer);
        this.subject$.next(this.list);
      }
    });
  }

  deleteCustomer(customer: Customer) {
    /**
     * Here we are updating our local array.
     * You would probably make an HTTP request here.
     */
    this.list.splice(this.list.findIndex((existingCustomer) => existingCustomer.id === customer.id), 1);
    this.selection.deselect(customer);
    this.subject$.next(this.list);
  }

  deleteCustomers(list: Customer[]) {
    /**
     * Here we are updating our local array.
     * You would probably make an HTTP request here.
     */
    list.forEach(c => this.deleteCustomer(c));
  }

  onFilterChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  onLabelChange(change: MatSelectChange, row: Customer) {
    const index = this.list.findIndex(c => c === row);
    this.list[index].labels = change.value;
    this.subject$.next(this.list);
  }
}
