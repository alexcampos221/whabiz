import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatIconModule } from "@angular/material/icon";
import { MatMenuModule } from "@angular/material/menu";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatSelectModule } from "@angular/material/select";
import { MatSortModule } from "@angular/material/sort";
import { MatTableModule } from "@angular/material/table";
import { MatTooltipModule } from "@angular/material/tooltip";
import { IconModule } from "@visurel/iconify-angular";
import { BreadcrumbsModule } from "src/@vex/components/breadcrumbs/breadcrumbs.module";
import { ConfigPanelModule } from "src/@vex/components/config-panel/config-panel.module";
import { PageLayoutModule } from "src/@vex/components/page-layout/page-layout.module";
import { SidebarModule } from "src/@vex/components/sidebar/sidebar.module";
import { ContainerModule } from "src/@vex/directives/container/container.module";
import { FooterModule } from "src/@vex/layout/footer/footer.module";
import { LayoutModule } from "src/@vex/layout/layout.module";
import { QuickpanelModule } from "src/@vex/layout/quickpanel/quickpanel.module";
import { SidenavModule } from "src/@vex/layout/sidenav/sidenav.module";
import { ToolbarModule } from "src/@vex/layout/toolbar/toolbar.module";
import { VexModule } from "src/@vex/vex.module";
import { AioTableRoutingModule } from "../../apps/aio-table/aio-table-routing.module";
// import { AioTableRoutingModule } from "../../apps/aio-table/aio-table-routing.module";
import { CustomerCreateUpdateModule } from "../../apps/aio-table/customer-create-update/customer-create-update.module";
import { UsuariosComponent } from "./usuarios.component";



@NgModule({
  declarations: [UsuariosComponent],
  imports: [
    CommonModule,
    AioTableRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    CustomerCreateUpdateModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatCheckboxModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    IconModule,
    FormsModule,
    MatTooltipModule,
    ReactiveFormsModule,
    ContainerModule,
    MatSelectModule,
    MatButtonToggleModule
  ]
})
export class UsuariosModule {
}
