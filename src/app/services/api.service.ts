import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { delay, map } from 'rxjs/operators';
import { Utils } from '../utils';
import * as Global from '../globals';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    public http: HttpClient,
    public userService: UserService
  ) { }
  login(body){
    // this.sweetalertsService.loading();
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    let request = this.http.post(Global.api_factor_500+ '/Whabiz/Usuario/login', body, requestOptions).pipe(
      // delay(1000),
      map((resp:any) => {
        this.userService.login(resp);
        // this.sweetalertsService.dissmissSwal();
        return resp;
      })
    )
    return request;
  }
}
