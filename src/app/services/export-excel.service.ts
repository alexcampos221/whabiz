import { Injectable } from '@angular/core';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class ExportExcelService {

  headersMask;
  constructor() { }

  ///////////////////////////BASIC/////////////////////////////////
  exportExcelBasic(excelData) {

    const title = excelData.title;
    const mapHeaders = excelData.mapHeaders;
    const mapHeadersShow = excelData.mapHeadersShow;
    const data = excelData.data;
    // this.headersMask = excelData.headersMask;
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet('Sales Data');

    // worksheet.getCell('A1').value = 'Filtro';worksheet.getCell('B1').value = excelData.filtro;worksheet.getCell('A1').font={bold:true}

    //add HEADERS
    let headers=[];
    mapHeadersShow.forEach(h=> {
      headers.push(h);
      console.info("headers: ",headers);
    });
    let headerRow = worksheet.addRow(headers);
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '4167B8' },
        bgColor: { argb: '' }
      }
      cell.font = {
        bold: true,
        color: { argb: 'FFFFFF' },
        size: 12
      }
    })
    worksheet.columns.forEach(column => {
      column.width = 24;
    })
    data.forEach((d) => {
      let row=[];
      mapHeadersShow.forEach(h=> {
        row.push(d[mapHeaders.get(h)]);
      });
      let currentRow=worksheet.addRow(row);
    });

    // worksheet.getColumn(3).width = 20;
    worksheet.addRow([]);

    // this.removeColumns(this.headersMask,removeCells,worksheet);
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, title + '.xlsx');
    })

  }
}