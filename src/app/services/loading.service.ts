import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  public loading$ = new Subject();

  constructor(
    public ngxSpinnerService: NgxSpinnerService,
  ) { }
  show(message?,opacity?){
    this.ngxSpinnerService.show();
    this.loading$.next({message:message,opacity:opacity?opacity:1});
  }
  hide(){
    this.ngxSpinnerService.hide();
  }
}
