import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }
  set(name, value){localStorage.setItem(name,JSON.stringify(value));}
  remove(name){localStorage.removeItem(name);}
  get(name){
    if(localStorage.getItem(name)){
      return JSON.parse(localStorage.getItem(name));
    }else{
      return undefined;
    }
  }
}
