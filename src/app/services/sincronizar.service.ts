import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { delay, map } from 'rxjs/operators';
import { Utils } from '../utils';
import * as Global from '../globals';

@Injectable({
  providedIn: 'root'
})
export class SincronizarService {

  constructor(
    public http: HttpClient,
  ) { }
  getQr(instance_url){
    // this.sweetalertsService.loading();
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    let request = this.http.get(instance_url+ '/whatsapp/init', requestOptions).pipe(
      // let request = this.http.get(Global.local_url+ '/whatsapp/init', requestOptions).pipe(
      // delay(1000),
      map((resp:any) => {
        // this.sweetalertsService.dissmissSwal();
        return resp;
      })
    )
    return request;
  }
  sincronize(instance_url){
    // this.sweetalertsService.loading();
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    let request = this.http.get(instance_url+ '/whatsapp/sincronize', requestOptions).pipe(
      // let request = this.http.get(Global.local_url+ '/whatsapp/sincronize', requestOptions).pipe(
      delay(1000),
      map((resp:any) => {
        console.info("sincronizee: ",resp);
        // this.sweetalertsService.dissmissSwal();
        return resp;
      })
    )
    return request;
  }
  getStatus(instance_url, body?){
    // this.sweetalertsService.loading();
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    let request = this.http.post(instance_url+ '/whatsapp/getStatus', body, requestOptions).pipe(
      // let request = this.http.post(Global.local_url+ '/whatsapp/getStatus', body, requestOptions).pipe(
      // delay(1000),
      map((resp:any) => {
        // this.sweetalertsService.dissmissSwal();
        return resp;
      })
    )
    return request;
  }
  logout(instance_url, body?){
    // this.sweetalertsService.loading();
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    let request = this.http.post(instance_url+ '/whatsapp/logout', body, requestOptions).pipe(
      // let request = this.http.post(Global.local_url+ '/whatsapp/logout', body, requestOptions).pipe(
      // delay(1000),
      map((resp:any) => {
        // this.sweetalertsService.dissmissSwal();
        return resp;
      })
    )
    return request;
  }
  restoreClient(instance_url, body){
    // this.sweetalertsService.loading();
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    let request = this.http.post(instance_url+ '/whatsapp/restoreClient', body, requestOptions).pipe(
      // let request = this.http.post(Global.local_url+ '/whatsapp/restoreClient', body, requestOptions).pipe(
      // delay(1000),
      map((resp:any) => {
        // this.sweetalertsService.dissmissSwal();
        return resp;
      })
    )
    return request;
  }
}
