import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import * as Global from '../globals';
const io = require("socket.io-client");

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  public events$ = new Subject();
  public qrExpirado$ = new Subject();
  public tokenEvent$ = new Subject();
  public clientInfo$ = new Subject();
  public authenticationInProgress$ = new Subject();
  private ioClient;
  constructor() { 

  }
  init(socket_url_device?){
    console.info("socketServiceInit");
    if(!this.ioClient){
      console.info("SOCKET NO EXISTE");
      this.ioClient = io.connect(socket_url_device?socket_url_device:Global.socket_url,{ transports: ['websocket', 'polling', 'flashsocket'] });
    }
    this.ioClient.on("auth_failure", (data) => {
      console.info("auth_failure: ",data);
      this.events$.next({event:"auth_failure",data:data});
    });
    this.ioClient.on("authenticated", (data) => {
      console.info("authenticated: ",data);
      this.events$.next({event:"authenticated",data:data});
    });
    this.ioClient.on("change_battery", (data) => {
      console.info("change_battery: ",data);
      this.events$.next({event:"change_battery",data:data});
    });
    this.ioClient.on("change_state", (data) => {
      console.info("change_state: ",data);
      this.events$.next({event:"change_state",data:data});
    });
    this.ioClient.on("disconnected", (data) => {
      console.info("disconnected: ",data);
      this.events$.next({event:"disconnected",data:data});
    });
    this.ioClient.on("group_join", (data) => {
      console.info("group_join: ",data);
      this.events$.next({event:"group_join",data:data});
    });
    this.ioClient.on("group_leave", (data) => {
      console.info("group_leave: ",data);
      this.events$.next({event:"group_leave",data:data});
    });
    this.ioClient.on("group_update", (data) => {
      console.info("group_update: ",data);
      this.events$.next({event:"group_update",data:data});
    });
    this.ioClient.on("incoming_call", (data) => {
      console.info("incoming_call: ",data);
      this.events$.next({event:"incoming_call",data:data});
    });
    this.ioClient.on("media_uploaded", (data) => {
      console.info("media_uploaded: ",data);
      this.events$.next({event:"media_uploaded",data:data});
    });
    this.ioClient.on("message", (data) => {
      console.info("message: ",data);
      this.events$.next({event:"message",data:data});
    });
    this.ioClient.on("message_ack", (data) => {
      console.info("message_ack: ",data);
      this.events$.next({event:"message_ack",data:data});
    });
    this.ioClient.on("message_create", (data) => {
      console.info("message_create: ",data);
      this.events$.next({event:"message_create",data:data});
    });
    this.ioClient.on("message_revoke_everyone", (data) => {
      console.info("message_revoke_everyone: ",data);
      this.events$.next({event:"message_revoke_everyone",data:data});
    });
    this.ioClient.on("message_revoke_me", (data) => {
      console.info("message_revoke_me: ",data);
      this.events$.next({event:"message_revoke_me",data:data});
    });
    this.ioClient.on("qr", (data) => {
      console.info("qr: ",data);
      this.events$.next({event:"qr",data:data});
    });
    this.ioClient.on("ready", (data) => {
      console.info("ready: ",data);
      this.events$.next({event:"ready",data:data});
    });
    //EXTRA EVENTS//////////
    this.ioClient.on("disconnected", (data) => {
      console.info("disconnected: ",data);
      this.events$.next({event:"disconnected",data:data});
    });
    this.ioClient.on("close_reason", (data) => {
      console.info("close_reason: ",data);
      this.events$.next({event:"close_reason",data:data});
    });
    this.ioClient.on("qr_expired", (data) => {
      console.info("qr_expired: ",data);
      this.events$.next({event:"qr_expired",data:data});
    });
    return this.ioClient;
  }

  end(){
    // ioClient?ioClient.disconnect():this.ioClient.disconnect();

    this.ioClient.disconnect();
    this.ioClient=undefined;
  }
}
