import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { delay, map } from 'rxjs/operators';
import * as Global from '../globals';
import { Utils } from '../utils';

@Injectable({
  providedIn: 'root'
})
export class TestService {

  constructor(
    public http: HttpClient,
  ) { }
  getParqueoByID(body){
    // this.sweetalertsService.loading();
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    let request = this.http.post(Global.api_url+ '/Parking/Parqueo/getParqueoByID', Utils.jsonToFormData(body), requestOptions).pipe(
      delay(1000),
      map((resp:any) => {
        // this.sweetalertsService.dissmissSwal();
        return resp;
      })
    )
    return request;
  }
  listarComprobantes(body){
    // this.sweetalertsService.loading();
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    let request = this.http.post(Global.api_url+ '/Parking/Parqueo/listarComprobantes', Utils.jsonToFormData(body), requestOptions).pipe(
      delay(1000),
      map((resp:any) => {
        // this.sweetalertsService.dissmissSwal();
        return resp;
      })
    )
    return request;
  }
}
