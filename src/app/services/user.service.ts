import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { LocalStorageService } from './local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public eventLogin$ = new Subject();
  constructor(
    private localStorageService: LocalStorageService
  ) { }
  login(data?){
    data?this.localStorageService.set('user',data):undefined;
    if(this.localStorageService.get('user')){
      this.eventLogin$.next(this.localStorageService.get('user'));
    }
  }
}
