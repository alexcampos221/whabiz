import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { delay, map } from 'rxjs/operators';
import { Utils } from '../utils';
import * as Global from '../globals';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor(
    public http: HttpClient,
  ) { }
  listarByEmpresa(body){
    // this.sweetalertsService.loading();
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    let request = this.http.post(Global.api_url+ '/Parking/Usuario/listarByEmpresa', Utils.jsonToFormData(body), requestOptions).pipe(
      delay(1000),
      map((resp:any) => {
        // this.sweetalertsService.dissmissSwal();
        return resp;
      })
    )
    return request;
  }
  guardar(body){
    // this.sweetalertsService.loading();
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    let request = this.http.post(Global.api_url+ '/Parking/Parqueo/guardar', Utils.jsonToFormData(body), requestOptions).pipe(
      delay(1000),
      map((resp:any) => {
        // this.sweetalertsService.dissmissSwal();
        return resp;
      })
    )
    return request;
  }
  editar(body){
    // this.sweetalertsService.loading();
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    let request = this.http.post(Global.api_url+ '/Parking/Parqueo/editar', Utils.jsonToFormData(body), requestOptions).pipe(
      delay(1000),
      map((resp:any) => {
        // this.sweetalertsService.dissmissSwal();
        return resp;
      })
    )
    return request;
  }
  whatsappClientGetInstance(body){
    // this.sweetalertsService.loading();
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    let request = this.http.post(Global.local_url+ '/whatsapp/getStatus', Utils.jsonToFormData(body), requestOptions).pipe(
      delay(1000),
      map((resp:any) => {
        // this.sweetalertsService.dissmissSwal();
        return resp;
      })
    )
    return request;
  }

}
