import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import * as Global from '../globals';

@Injectable({
  providedIn: 'root'
})
export class WhatsappService {

  constructor(
    public http: HttpClient
  ) { }
  sendMessageText(body){
    // this.sweetalertsService.loading();
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    let request = this.http.post(Global.local_url+ '/whatsapp/sendMessageText', body, requestOptions).pipe(
    // let request = this.http.post(Global.producer_url+ '/producer/sendMessageText', body, requestOptions).pipe(
      // delay(1000),
      map((resp:any) => {
        // this.sweetalertsService.dissmissSwal();
        return resp;
      })
    )
    return request;
  }
  sendMessageMediaUrl(body){
    // this.sweetalertsService.loading();
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    let request = this.http.post(Global.producer_url+ '/producer/sendMessageMediaUrl', body, requestOptions).pipe(
      // delay(1000),
      map((resp:any) => {
        // this.sweetalertsService.dissmissSwal();
        return resp;
      })
    )
    return request;
  }
  sendMessageFileUrl(body){
    // this.sweetalertsService.loading();
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    let request = this.http.post(Global.producer_url+ '/producer/sendMessageFileUrl', body, requestOptions).pipe(
      // delay(1000),
      map((resp:any) => {
        // this.sweetalertsService.dissmissSwal();
        return resp;
      })
    )
    return request;
  }
  sendMessageStickerUrl(body){
    // this.sweetalertsService.loading();
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    let request = this.http.post(Global.producer_url+ '/producer/sendMessageStickerUrl', body, requestOptions).pipe(
      // delay(1000),
      map((resp:any) => {
        // this.sweetalertsService.dissmissSwal();
        return resp;
      })
    )
    return request;
  }
  currentClients(body){
    // this.sweetalertsService.loading();
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    console.info("currentClientsService");
    let request = this.http.post(Global.local_url+ '/whatsapp/currentClients', body, requestOptions).pipe(
      // delay(1000),
      map((resp:any) => {
        // this.sweetalertsService.dissmissSwal();
        return resp;
      })
    )
    return request;
  }
  setStatus(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    console.info("currentClientsService");
    let request = this.http.post(Global.local_url+ '/whatsapp/setStatus', body, requestOptions).pipe(
      // delay(1000),
      map((resp:any) => {
        // this.sweetalertsService.dissmissSwal();
        return resp;
      })
    )
    return request;
  }
  setDisplayName(body){
    const requestOptions = {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    console.info("currentClientsService");
    let request = this.http.post(Global.local_url+ '/whatsapp/setDisplayName', body, requestOptions).pipe(
      // delay(1000),
      map((resp:any) => {
        // this.sweetalertsService.dissmissSwal();
        return resp;
      })
    )
    return request;
  }
}
