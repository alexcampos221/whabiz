// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyAVIUj2USouSgV6dnHIctUl_gkmsOR3Sng",
    authDomain: "whabiz-new.firebaseapp.com",
    projectId: "whabiz-new",
    storageBucket: "whabiz-new.appspot.com",
    messagingSenderId: "100529715329",
    appId: "1:100529715329:web:133a9250edd4cf8761f580",
    measurementId: "G-DN8FECS7HC"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
